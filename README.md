# Pan y Queso



## Version

Primera versión funcional (1.0)v

## Getting started

Para comenzar a utilizar el programa basta simplemente con colocar ambos archivos (index.html y script.js) en un mismo directorio y ejecutar el primero en un navegador web.
Se le pedirán a los usuarios los siguientes datos:

* Nombre y talla de calzado al participante 1
* Nombre y talla de calzado al participante 2
* No se permiten campos vacíos
* No se permiten talles menores a 34.5 ni mayores e iguales a 44

Una vez igresados los datos, basta con presionar el botón "Empezar" y el programa comenzará a funcionar.

* Se establecerá automáticamente y de forma aleatoria el rol de cada participante (Pan y Queso)
* Se establecerá automáticamente una distancia de juego de 10 metros hasta 20 metros inclusive.

El ganador será el jugador que llegue a pisar el pie del otro jugador, lo que sería la distancia total más 5 centímetros extra para abarcar más allá de la punta del pie del contrincante.
